import http from 'http';
import express from 'express';
import bodyparser from 'body-parser';
import ejs from 'ejs';
import { router } from './router/index.js';
import { fileURLToPath } from 'url';
import path from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const app = express();
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));
app.use(bodyparser.urlencoded({ extended: true }));

// Cambiar extensiones HTML a EJS
app.engine('html', ejs.renderFile);
app.use(router);

// Pagina de error
app.use((req, res, next) => {
  res.status(404).sendFile(__dirname + '/public/error.html');
});
const puerto = 3000;
app.listen(puerto, () => {
  console.log('Servidor escuchando en el puerto ', puerto);
});

