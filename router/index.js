const express = require("express");
const router = express.Router();
const bodyparser = require("body-parser");

router.get("/",(req,res)=>{
    
    const valores = {
        numRecibo : req.query.numRecibo,
        nomCliente : req.query.nomCliente ,
        domicilio : req.query.domicilio,
        tipo : req.query.tipo,
        PagoD : req.query.PagoD,
        subtotal : req.query.subtotal,
		impuesto : req.query.impuesto,
		total: req.query.total,
        DiasT: req.query.DiasT,
        numContrato: req.query.numContrato,
        nombre: req.query.nombre
    }
    

    res.render('paginaEntrada.html',valores)

})

router.post('/', (req, res) => {
    const params = {
      numContrato: req.body.numContrato,
      nombre: req.body.nombre,
      domicilio: req.body.domicilio,
      estudios: req.body.tipo,
      pagoDiario: req.body.pagoDiario,
      dias: req.body.DiasT,
    };
  
    res.render('PaginaSalida.html', params);
  });
  

module.exports=router;